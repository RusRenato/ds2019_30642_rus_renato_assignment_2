package ro.utcluj.assignment1.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.utcluj.assignment1.model.Activity;
import ro.utcluj.assignment1.model.Role;
import ro.utcluj.assignment1.model.User;
import ro.utcluj.assignment1.service.UserService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
public class FileReader {

    @Autowired
    UserService userService;

    public List<Activity> read(){
        AtomicInteger i = new AtomicInteger(0);
        AtomicInteger id = new AtomicInteger(0);
        List<Activity> monitoredDataList = new ArrayList<>();
        List<Integer> userIds = userService.getAllUsers().stream().filter(user -> user.getRol() == Role.Patient).map(User::getId).collect(Collectors.toList());
        Random random = new Random();

        try (Stream<String> stream = Files.lines(Paths.get("C:/Users/Rus Renato/Desktop/Anul 4 Semestrul 1/DS/Lab/assignment1/back-end/activity.txt"))) {

            stream.forEach(s -> {
                String[] pair = s.split("[\t]+");
                Activity activity = new Activity();
                activity.setActivity(pair[2]);
                activity.setStart(pair[0]);
                activity.setEnd(pair[1]);
                if (i.get() == 0) {
                    id.set(userIds.get(random.nextInt(userIds.size())));
                }
                activity.setId(id.get());
                if(i.get() < 3) {
                    i.getAndIncrement();
                } else if(i.get() == 3) {
                    i.set(0);
                }
                monitoredDataList.add(activity);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return monitoredDataList;
    }

}
