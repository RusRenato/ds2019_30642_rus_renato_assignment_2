package ro.utcluj.assignment1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Component
public class WebSocketController {

    private final SimpMessagingTemplate template;

    @Autowired
    WebSocketController(SimpMessagingTemplate template) {
        this.template = template;
    }

    @SendTo("/send/message")
    public void onReceivedMessage(String message, int id) {
        System.out.println("Am trimis " + System.currentTimeMillis());
        this.template.convertAndSend("/chat/" + id, message);
    }

}
